### Temperatures

Sample dockerized Java application. It requires Java 8, Maven 3.5.0 and Docker 17.

In order to run it, follow these steps:

1. mvn package
2. docker build -t littlefriend/temperature:latest .
3. docker run littlefriend/temperature:latest