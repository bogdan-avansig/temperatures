package com.exercise.temperature.util;

import com.exercise.temperature.model.Temperature;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class TemperatureUtilsTest {

    private List<Temperature> temperatures;

    private TemperatureUtils testee;

    @Before
    public void setUp() {
        this.temperatures = Arrays.asList(
                new Temperature(23.4),
                new Temperature(16.7),
                new Temperature(21.6),
                new Temperature(19.5));
        this.testee = new TemperatureUtils();
    }

    @Test
    public void shouldReturnEmptyOptionalForAverageOfEmptyOrNullList() throws Exception {
        OptionalDouble average = testee.calculateAverageTemp(new ArrayList<Temperature>(0));
        Assert.assertFalse(average.isPresent());
    }

    @Test
    public void shouldReturnAverageTemperature() throws Exception {
        OptionalDouble avgTemp = testee.calculateAverageTemp(temperatures);
        Assert.assertEquals(20.3, avgTemp.getAsDouble(), 0.00);
    }

    @Test
    public void shouldReturnEmptyOptionalForMedianOfEmptyOrNullList() throws Exception {
        OptionalDouble average = testee.calculateMedianTemp(new ArrayList<Temperature>(0));
        Assert.assertFalse(average.isPresent());
    }

    @Test
    public void shouldReturnMedianTemperatureAsAverageWhenEvenElements() throws Exception {
        OptionalDouble medianTemp = testee.calculateMedianTemp(temperatures);
        Assert.assertEquals(20.55, medianTemp.getAsDouble(), 0.00);
    }

    @Test
    public void shouldReturnMedianTemperatureAsMiddleValueWhenOddElements() throws Exception {
        temperatures = Arrays.asList(
                new Temperature(23.4),
                new Temperature(16.7),
                new Temperature(21.6),
                new Temperature(19.5),
                new Temperature(21.6)
        );
        OptionalDouble medianTemp = testee.calculateMedianTemp(temperatures);
        Assert.assertEquals(21.6, medianTemp.getAsDouble(), 0.00);
    }

    @Test
    public void shouldReturnEmptyListJsonForEmptyList() throws Exception {
        String json = testee.jsonRepresentation(new ArrayList<>(0));
        Assert.assertEquals("[]", json);
    }

    @Test
    public void shouldReturnJsonRepresentation() throws Exception {
        String json = testee.jsonRepresentation(temperatures);
        Assert.assertEquals("[{\"temperature\":23.4},{\"temperature\":16.7},{\"temperature\":21.6},{\"temperature\":19.5}]", json);
    }

}
