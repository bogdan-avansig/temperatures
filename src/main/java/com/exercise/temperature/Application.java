package com.exercise.temperature;

import com.exercise.temperature.model.Temperature;
import com.exercise.temperature.util.TemperatureUtils;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Arrays;
import java.util.List;

public class Application {

    public static void main(String[] args) throws JsonProcessingException {
        final TemperatureUtils temperatureUtils = new TemperatureUtils();

        final List<Temperature> temperatures = Arrays.asList(
                new Temperature(23.4),
                new Temperature(16.7),
                new Temperature(21.6),
                new Temperature(19.5));

        System.out.println("Average temperature: " + temperatureUtils.calculateAverageTemp(temperatures));

        System.out.println("Median temperature: " + temperatureUtils.calculateMedianTemp(temperatures));

        System.out.println("JSON: " + temperatureUtils.jsonRepresentation(temperatures));
    }
}
