package com.exercise.temperature.util;

import com.exercise.temperature.model.Temperature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class TemperatureUtils {

    public OptionalDouble calculateAverageTemp(List<Temperature> temperatures) {
        if (temperatures == null || temperatures.isEmpty()){
            return OptionalDouble.empty();
        }
        return temperatures.stream().mapToDouble(Temperature::getTemperature).average();
    }

    public OptionalDouble calculateMedianTemp(List<Temperature> temperatures) {
        if (temperatures == null || temperatures.isEmpty()){
            return OptionalDouble.empty();
        }
        List<Double> sortedTemperatures = temperatures.stream()
                .mapToDouble(Temperature::getTemperature)
                .sorted()
                .boxed()
                .collect(Collectors.toList());
        final int size = sortedTemperatures.size();
        if (sortedTemperatures.size() % 2 == 0){
            return OptionalDouble.of((sortedTemperatures.get(size / 2) + sortedTemperatures.get((size / 2) - 1)) / 2);
        } else {
            return OptionalDouble.of(sortedTemperatures.get(size / 2));
        }
    }

    public String jsonRepresentation(List<Temperature> temperatures) throws JsonProcessingException {
        if (temperatures == null || temperatures.isEmpty()) {
            return "[]";
        }
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(temperatures);
    }
}
