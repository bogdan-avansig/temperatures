FROM openjdk:8-jdk
MAINTAINER <bogdan.utanu@gmail.com>

COPY target/temperature-1.0-SNAPSHOT-jar-with-dependencies.jar /app/temperature.jar

CMD ["java", "-jar", "/app/temperature.jar"]


